window.addEventListener("load", function () {
    /* Base address for the Pokemon endpoints. Add the endpoint name and parameters onto this */
    const ENDPOINT_BASE_URL = "https://trex-sandwich.com/pokesignment/";
    const POKEMON_RANDOM = "https://trex-sandwich.com/pokesignment/pokemon?random=random";
    const POKEMON_IMAGE_ROOT = "https://trex-sandwich.com/pokesignment/img/";
    const POKEMON_NAMES = "https://trex-sandwich.com/pokesignment/pokemon";
    const POKEMON_CLASS_COLOR = "https://sporadic.nz/pokesignment/keyword?keyword=";

    /* TODO: Your code here */
    let navHamburger = document.querySelector('.navigation_hamburger');
    let image = document.querySelector("#Pokemon_of_the_Day_image");

    navHamburger.addEventListener('click', function () {
        document.querySelectorAll('.navigation_list').forEach(element => element.classList.toggle('navigation_hamburger_list'));
    });

    //part 2 -- populate random pokemon
    display_Pokemon_of_the_Day_content();
    image.addEventListener("click", display_Pokemon_of_the_Day_content);

    async function display_Pokemon_of_the_Day_content() {
        let randomResponse = await fetch(POKEMON_RANDOM);
        let randomJsonObject = await randomResponse.json();

        let name = document.querySelector("#Pokemon_of_the_Day_name");
        let description = document.querySelector("#Pokemon_of_the_Day_description")
        image.setAttribute("src", POKEMON_IMAGE_ROOT + randomJsonObject.image);
        // image.style.maxWidth = "100%";
        name.innerHTML = randomJsonObject.name;
        description.innerHTML = randomJsonObject.description;
    }

    //part 3 -- populate Pokemon details
    async function getAllPokemonNames() {
        let allPokemonNamesResponse = await fetch(POKEMON_NAMES);
        return await allPokemonNamesResponse.json();
    }

    async function getPokemonByName (name){
        let pokemon = await fetch(ENDPOINT_BASE_URL + `pokemon?pokemon=` + name);
        return pokemon.json();
    }

    async function allPokemon(){
        let pokemonNames = await getAllPokemonNames();
        let pokemonArray = new Array();
        for (const p of pokemonNames){
            let pokemon = await fetch(ENDPOINT_BASE_URL + `pokemon?pokemon=` + p);
            await pokemonArray.push(await pokemon.json());
        }
        return pokemonArray;
    }

    function displayPokemonCard(pokemon) {
        let pokemonInformation = document.createElement("div");
        pokemonInformation.classList.add(`pokemonInformation`);
        pokemonInformation.id = `${pokemon.name}`;
        let pokemonImage = document.createElement("img");
        pokemonImage.setAttribute("src", POKEMON_IMAGE_ROOT + pokemon.image);
        // pokemonImage.style.maxWidth = "100%";
        let pokemonName = document.createElement("p");
        pokemonName.classList.add(`name`);
        pokemonName.innerHTML = pokemon.name;
        pokemonName.style.fontWeight = "bold";

        pokemonInformation.appendChild(pokemonImage);
        pokemonInformation.appendChild(pokemonName);

        return pokemonInformation;
    }

    async function displayAllPokemon() {
        let pokemonArray = await allPokemon();
        let pokemon_details = document.querySelector("#Pokemon_details");
        let pokemonContainer = document.querySelector("#pokemon_content");
        pokemonContainer.classList.add(`pokemonContainer`);

        let pokemonCard = document.createElement("div");
        pokemonCard.classList.add(`pokemonCard`);

        for (let p of pokemonArray){
            let pokemonCard = await displayPokemonCard(p)
            pokemonContainer.appendChild(pokemonCard);
            pokemon_details.appendChild(pokemonContainer);
            pokemonCard.onclick = async function () {
                await displayChosenPokemonDetails(p.name);
            }
        }
    }

    displayAllPokemon();

    async function displayChosenPokemonDetails(name) {
        let chosenPokemon = await getPokemonByName(name);
        let weakPokemon1 = await getPokemonByName(chosenPokemon.opponents.weak_against[0]);
        let weakPokemon2 = await getPokemonByName(chosenPokemon.opponents.weak_against[1]);
        let strongPokemon1 = await getPokemonByName(chosenPokemon.opponents.strong_against[0]);
        let strongPokemon2 = await getPokemonByName(chosenPokemon.opponents.strong_against[1]);

        let pokemonContainer = document.querySelector("#pokemon_content");
        pokemonContainer.innerHTML = `<div id="weakAgainst"></div>`+ `<div id="detailsOfPokemon"></div>` +
            `<div id="strongAgainst"></div>`;

        let weakAgainstPokemon = document.querySelector("#weakAgainst");
        let strongAgainstPokemon = document.querySelector("#strongAgainst");
        let detailsOfPokemon = document.querySelector("#detailsOfPokemon");

        detailsOfPokemon.innerHTML = `<div class="chosenPokemon"><h2>${chosenPokemon.name}</h2></div>`
            + `<div><img id="centerImage" src="${POKEMON_IMAGE_ROOT}${chosenPokemon.image}">` + `<p>${chosenPokemon.description}</p></div>`;
        let centerImage = document.querySelector("#centerImage");
        // centerImage.style.maxWidth = "100%";
        await weakAgainstPokemon.appendChild(displayPokemonCard(weakPokemon1));
        await weakAgainstPokemon.appendChild(displayPokemonCard(weakPokemon2));
        await strongAgainstPokemon.appendChild(displayPokemonCard(strongPokemon1));
        await strongAgainstPokemon.appendChild(displayPokemonCard(strongPokemon2));

        let pokemon = document.querySelectorAll(".pokemonInformation");
        pokemon.forEach(p => p.onclick = async function(){
            await displayChosenPokemonDetails(p.id);
        })

        pokemonContainer.classList.remove(`pokemonContainer`);
        pokemonContainer.classList.add(`clickedPokemonContainer`);
    }

    //part 4
    async function getPokemonClass(pokomen) {
        let classList = document.createElement("div");
        classList.id = "pokemonClass";
        classList.innerHTML = `<p>Class List</p>`;
        for (const pClass of pokomen.classes){
            let pClassResponse = await fetch(`${POKEMON_CLASS_COLOR}${pClass}`);
            let pClassColor = await pClassResponse.json();

            let pokemonClass = document.createElement("div");

        }

    }


});

