window.addEventListener("load", function () {
    /* Base address for the Pokemon endpoints. Add the endpoint name and parameters onto this */
    const ENDPOINT_BASE_URL = "https://trex-sandwich.com/pokesignment/";
    const POKEMON_RANDOM = "https://trex-sandwich.com/pokesignment/pokemon?random=random";
    const POKEMON_IMAGE_ROOT = "https://trex-sandwich.com/pokesignment/img/";

    /* TODO: Your code here */
    let navHamburger = document.querySelector('.navigation_hamburger');
    let image = document.querySelector("#Pokemon_of_the_Day_image");

    navHamburger.addEventListener('click', function () {
        document.querySelectorAll('.navigation_list').forEach(element => element.classList.toggle('navigation_hamburger_list'));
    });

    //part 2 -- populate random pokemon
    display_Pokemon_of_the_Day_content();
    image.addEventListener("click", display_Pokemon_of_the_Day_content);

    async function display_Pokemon_of_the_Day_content() {
        let randomResponse = await fetch(POKEMON_RANDOM);
        let randomJsonObject = await randomResponse.json();

        let name = document.querySelector("#Pokemon_of_the_Day_name");
        let description = document.querySelector("#Pokemon_of_the_Day_description")
        image.setAttribute("src", POKEMON_IMAGE_ROOT + randomJsonObject.image);
        image.style.maxWidth = "100%";
        name.innerHTML = randomJsonObject.name;
        description.innerHTML = randomJsonObject.description;
    }

});

